--------------------------PfPCS-------------------------
-----------------------Labwork #2-----------------------
----------Ada. Protected modules------------------------
--------------------------------------------------------
--Task: a = min(f * B + d * C * ( MO * MK ))------------
--------------------------------------------------------
--Author: Botvinko Roman, IO-53 group-------------------
--Date: 10.05.2018--------------------------------------
--------------------------------------------------------

with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;
package body data is
   
procedure Input_Integer(a: out Integer) is

begin
      a := 1;
end Input_Integer;

procedure Input_Vector(A: access Vector) is

begin
      for i in 1..N loop
         A(i) := 1;
      end loop;
   end Input_Vector;

procedure Input_Matrix(MA: access Matrix) is

begin
      for i in 1..N loop
         for j in 1..N loop
            MA(i)(j) := 1;
         end loop;
      end loop;
   end Input_Matrix;

   procedure Output_Integer(a: in Integer) is
   begin
      New_Line;
      Put(a);
      New_Line;
   end Output_Integer;

function Multiply_Matrixes(MA: access Matrix; MB: access Matrix; k: in Integer) return access Matrix is
   	cell: Integer;
	result: access Matrix := new Matrix;
begin
	for i in H*(k - 1) + 1..H * k loop
		for j in 1..N loop
			cell := 0;
			for l in 1..N loop
                            cell := cell + MA(i)(l) * MB(l)(j);
			end loop;
                    result(i)(j) := cell;
		end loop;
	end loop;
	return result;
   end Multiply_Matrixes;
   
function Multiply_Vector_Matrix(MA: access Matrix; A: access Vector; k: in Integer) return access Vector is
      cell: Integer;
      result: access Vector := new Vector;
   begin
      for i in H*(k - 1) + 1..H * k loop
         cell := 0;
         for j in 1..N loop
            cell := cell + A(j) * MA(i)(j);
         end loop;
         result(i) := cell;
      end loop;
      return result;
   end Multiply_Vector_Matrix;
   
function Multiply_Vector_Integer(A: access Vector; b: in Integer; k: in Integer) return access Vector is
      result: access Vector := new Vector;
   begin
      for i in h*(k - 1) + 1..h * k loop
         result(i) := A(i) * b;
      end loop;
      return result;
   end Multiply_Vector_Integer;
   
procedure Sum_Vectors(A: access Vector; B: access Vector; C: access Vector; k: in Integer) is
   begin
      for i in H*(k - 1) + 1..H * k loop
         C(i) := A(i) + B(i);
      end loop;
   end Sum_Vectors;
   
function Min_Vector(A: access Vector; k: in Integer) return Integer is
	result: integer;
begin
	result := A(H*(k - 1) + 1);
      for i in H*(k - 1) + 1..H * k loop
		if A(i) < result then
			result := A(i);
		end if;
	end loop;
	return result;
   end Min_Vector;
   
end data;
