--------------------------PfPCS-------------------------
-----------------------Labwork #2-----------------------
----------Ada. Protected modules------------------------
--------------------------------------------------------
--Task: a = min(f * B + d * C * ( MO * MK ))------------
--------------------------------------------------------
--Author: Botvinko Roman, IO-53 group-------------------
--Date: 10.05.2018--------------------------------------
--------------------------------------------------------

with Ada.Text_IO, Ada.Integer_Text_IO, Ada.Calendar, Ada.Unchecked_Deallocation;
use Ada.Text_IO, Ada.Integer_Text_IO, Ada.Calendar;
with data;

procedure Lab5 is
   N: Integer := 8;
   P: Integer := 4;
   package using_data is new data(N, P);
   use using_data;

   --variables
   Z: access Vector := new Vector;
   B: access Vector := new Vector;
   MK: access Matrix := new Matrix;

   --protected module Module_Control
   protected Module_Control is
      entry Wait_Input;
      entry Wait_Calculation;

      procedure Signal_Input;
      procedure Signal_Calculation;

      procedure Set_d;
      procedure Set_f;
      procedure Set_C;
      procedure Set_MO;
      procedure Set_Min(c: in Integer);
      function Get_a return Integer;
      function Get_d return Integer;
      function Get_f return Integer;
      function Get_C return access Vector;
      function Get_MO return access Matrix;

   private
      a: Integer := 100;
      d: Integer;
      f: Integer;
      C: access Vector := new Vector;
      MO: access Matrix := new Matrix;
      F1: Natural := 0;
      F2: Natural := 0;
      F3: Natural := 0;
   end Module_Control;

   protected body Module_Control is
      entry Wait_Input when F1 = 1 is
      begin
         null;
      end Wait_Input;

      entry Wait_Calculation when F2 = P is
      begin
         null;
      end Wait_Calculation;

      procedure Signal_Input is
      begin
         F1 := F1 + 1;
      end Signal_Input;

      procedure Signal_Calculation is
      begin
         F2 := F2 + 1;
      end Signal_Calculation;

      procedure Set_Min(c: in Integer) is
      begin
         a := Integer'Min(a, c);
      end Set_Min;

      procedure Set_f is
      begin
         Input_Integer(f);
      end Set_f;

      procedure Set_d is
      begin
         Input_Integer(d);
      end Set_d;

      procedure Set_C is
      begin
         Input_Vector(C);
      end Set_C;

      procedure Set_MO is
      begin
         Input_Matrix(MO);
      end Set_MO;

      function Get_a return Integer is
      begin
         return a;
      end Get_a;

      function Get_d return Integer is
      begin
         return d;
      end Get_d;

      function Get_f return Integer is
      begin
         return f;
      end Get_f;

      function Get_C return access Vector is
      begin
         return C;
      end Get_C;

      function Get_MO return access Matrix is
      begin
         return MO;
      end Get_MO;

   end Module_Control;

   task type Tasks (Tid: Integer);

   task body Tasks is
      ai: Integer;
      di: Integer;
      fi: Integer;
      Ci: access Vector := new Vector;
      MOi: access Matrix := new Matrix;
   begin
      Put_Line("T" & Integer'Image(Tid) & " started");

      --1.Input MT, MO in T1
      if Tid = 1 then
         Input_Vector(B);
         Input_Matrix(MK);
         Module_Control.Set_d;
         Module_Control.Set_f;
         Module_Control.Set_C;
         Module_Control.Set_MO;

         --2.Signal about end of input in T1
         Module_Control.Signal_Input;
      end if;

      --3.Wait for data input in T1
      Module_Control.Wait_Input;

      --4.Copying critical variables
      fi := Module_Control.Get_f;
      di := Module_Control.Get_d;
      Ci := Module_Control.Get_C;
      MOi := Module_Control.Get_MO;

      --5.Calculation of the function
      Sum_Vectors(Multiply_Vector_Integer(B,fi,Tid),Multiply_Vector_Integer(Multiply_Vector_Matrix(Multiply_Matrixes(MK,MOi,Tid),Ci,Tid),di,Tid),Z,Tid);

      --8.Calculation of local min
      ai := Min_Vector(Z,Tid);

      --9.Calculation of global min
      Module_Control.Set_Min(ai);

      --10.Signal about end of calculation min
      Module_Control.Signal_Calculation;

      if Tid = 1 then
         --11.Wait for the end of calculation of min
         Module_Control.Wait_Calculation;

         --15.Output result
         Output_Integer(Module_Control.Get_a);
      end if;

      Put_Line("T" & Integer'Image(Tid) & " finished");
   end Tasks;

   type Task_Ptr is access Tasks;
   Tasks_arr: array (1..P) of Task_Ptr;

   --procedure to deallocate task after finishing
   procedure Free_Tasks is new Ada.Unchecked_Deallocation(Tasks, Task_Ptr);

begin
   Put_Line("Labwork #2 started");
   for i in 1..P loop
      Tasks_arr(i) := new Tasks(i);
   end loop;
   Module_Control.Wait_Calculation;
   delay(1.0);
   for i in 1..P loop
      Free_Tasks(Tasks_arr(i));
   end loop;
   Put_Line("Labwork #2 finished");
end Lab5;
