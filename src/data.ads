--------------------------PfPCS-------------------------
-----------------------Labwork #2-----------------------
----------Ada. Protected modules------------------------
--------------------------------------------------------
--Task: a = min(f * B + d * C * ( MO * MK ))------------
--------------------------------------------------------
--Author: Botvinko Roman, IO-53 group-------------------
--Date: 10.05.2018--------------------------------------
--------------------------------------------------------

generic
      N: in Integer;
      P: in Integer;
package data is
   
   type Vector is array (1..N) of Integer;
   type Matrix is array (1..N) of Vector;
   H: Integer := N / P;
   
   --Input Integer, Vector, Matrix, atomic Vector, atomic Matrix
   procedure Input_Integer(a: out Integer);
   procedure Input_Vector(A: access Vector);
   procedure Input_Matrix(MA: access Matrix);
   
   --Output Matrix
   procedure Output_Integer(a: in Integer);
   
   --Multiply functions
   function Multiply_Matrixes(MA: access Matrix; MB: access Matrix; k: in Integer) return access Matrix;
   function Multiply_Vector_Matrix(MA: access Matrix; A: access Vector; k: in Integer) return access Vector;
   function Multiply_Vector_Integer(A: access Vector; b: in Integer; k: in Integer) return access Vector;
   
   --Sum procedure
   procedure Sum_Vectors(A: access Vector; B: access Vector; C: access Vector; k: in Integer);
   
   --Min function
   function Min_Vector(A: access Vector; k: in Integer) return Integer;
   
end data;
